/**
 * Created by lizuyao on 3/3/15.
 */
import edu.berkeley.nlp.lm.NgramLanguageModel;
import edu.berkeley.nlp.lm.io.LmReaders;
import edu.berkeley.nlp.lm.util.Logger;

import java.io.*;
import java.util.*;

public class CalculateScore {
    public static Map<String,String> dict=new HashMap<String, String>();
    private static void createDictionary()
    {
        dict.put("they're","their");
        dict.put("their","they're");
        dict.put("its","it's");
        dict.put("it's","its");
        dict.put("you're", "your");
        dict.put("your", "you're");
//        dict.put("They're","Their");
//        dict.put("Their","They're");
//        dict.put("Its","It's");
//        dict.put("It's","Its");
//        dict.put("You're", "Your");
//        dict.put("Your", "You're");
        dict.put("loose", "lose");
        dict.put("lose","loose");
        dict.put("to","too");
        dict.put("too","to");
    }
    private static void usage() {
        System.err.println("Usage: <Berkeley LM binary file> <outputfile>*\nor\n-g <vocab_cs file> <Google LM Binary>");
        System.exit(1);
    }
//    private static List<List<String>> getWindowOfWords(String sentence)
//    {
//        List<List<String>> result = new ArrayList<List<String>>();
//        List<String> words = Arrays.asList(sentence.trim().split("\\s+"));
//        for (int i=0; i< words.size(); i++)
//        {
//            if (dict.containsKey(words.get(i)))
//            {
//                int startIndex=i-2;
//                int endIndex=i+3;
//                if (startIndex<0) {
//                    startIndex=0;
//                }
//                if (endIndex>=words.size())
//                {
//                    endIndex=words.size();
//                }
//                result.add(words.subList(startIndex, endIndex));
//            }
//        }
//        return result;
//    }
    private static String correct(List<String> words, NgramLanguageModel<String> lm)
    {
        List<String> newWords= new ArrayList<String>();
        int windSize=2;
        for (int i=0; i< words.size(); i++)
        {
            if (dict.containsKey(words.get(i)))
            {
                int startIndex=i-windSize;
                int endIndex=i+windSize+1;
                if (startIndex<0) {
                    startIndex=0;
                }
                if (endIndex>=words.size())
                {
                    endIndex=words.size();
                }
                List<String> window = words.subList(startIndex,endIndex);
                double oldScore=lm.scoreSentence(window);
                String key = words.get(i);
                List<String> newWindow = new ArrayList<String>();
                for (String s : window)
                {
                    if (s.equals(key))
                        newWindow.add(dict.get(key));
                    else
                        newWindow.add(s);
                }
                double newScore=lm.scoreSentence(newWindow);
                // flip word
                if (newScore > oldScore)
                    newWords.add(dict.get(key));
                else
                    newWords.add(key);
            }
            else {
                newWords.add(words.get(i));
            }
        }
        return String.join(" ",newWords);
        
    }
    public static void main(String[] argv) throws IOException {
        int i = 0;
        if (i >= argv.length) usage();
        boolean isGoogleBinary = false;
        if (argv[i].equals("-g")) {
            isGoogleBinary = true;
            i++;
        }
        if (i >= argv.length) usage();
        String vocabFile = null;
        if (isGoogleBinary) {
            vocabFile = argv[i++];
        }
        if (i >= argv.length) usage();
        String binaryFile = argv[i++];
        NgramLanguageModel<String> lm = readBinary(isGoogleBinary, vocabFile, binaryFile);
        createDictionary();
        //PrintWriter writer = new PrintWriter("../dev/hw3.dev.out.txt", "UTF-8");
//        String file="../dev/hw3.dev.err.txt";
//        BufferedReader br = new BufferedReader(new FileReader(file));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = br.readLine()) != null) {
            // process the line.
            List<String> words = Arrays.asList(line.trim().split("\\s+"));
            String newSentence=correct(words, lm);
            if (!newSentence.isEmpty()) {
                System.out.print(newSentence);
                System.out.println(' ');
            }
            else
                System.out.println();
        }
        br.close();
        //writer.close();
    }
    private static NgramLanguageModel<String> readBinary(boolean isGoogleBinary, String vocabFile, String binaryFile) {
        NgramLanguageModel<String> lm = null;
        if (isGoogleBinary) {
            Logger.startTrack("Reading Google Binary " + binaryFile + " with vocab " + vocabFile);
            lm = LmReaders.readGoogleLmBinary(binaryFile, vocabFile);
            Logger.endTrack();
        } else {
            Logger.startTrack("Reading LM Binary " + binaryFile);
            lm = LmReaders.readLmBinary(binaryFile);
            Logger.endTrack();
        }
        return lm;
    }
}
