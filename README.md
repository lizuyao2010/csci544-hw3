#Accuracy
    I used diff command to compare how many lines are different from my output file with gold correct file which is 190 and how many orginal error file with gold correct file which is 6098.
    the accuracy=1-190/6098=0.9688
#Method
    I used Trigram language model.
    I define trigger words(like "you're" and "your") for my error detection program. 
    If my programm see a word like "you're", it get the surrounding words(previous two and next two) to calculate the log probability of this 5 words sequence and also calculate the sequence has the same surrounding words but replace "you're" with "your".
#Corpus
    latest english Wiki pedia
#Third-party software used
    Berkeley Language Model
#Usage
    cat hw3.test.err.txt | java -Xmx4g -cp out/production/homework3:lib/berkeleylm.jar CalculateScore new_wiki.binary > hw3.output.txt
    